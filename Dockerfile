FROM debian:stretch

ENV DEBIAN_FRONTEND noninteractive

# Prerequisites
RUN printf "#!/bin/sh\nexit 101" > /usr/sbin/policy-rc.d && chmod a+x /usr/sbin/policy-rc.d \
    && \
    apt-get update \
    && \
    apt-get install -y locales && \
    printf "pl_PL.UTF-8 UTF-8\nen_US.UTF-8 UTF-8" > /etc/locale.gen && dpkg-reconfigure locales \
    && \
    apt-get install -y --no-install-recommends \
    syslog-ng \
    cron \
    logrotate \
    procps \
    vim-tiny \
    less \
    lnav \
    multitail \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    echo "#!/bin/sh\nexit 0" > /usr/sbin/policy-rc.d

# locales
RUN printf "pl_PL.UTF-8 UTF-8\nen_US.UTF-8 UTF-8" > /etc/locale.gen && dpkg-reconfigure locales

VOLUME ["/var/log"]

RUN adduser --gecos "" --disabled-password loguser && rm -f /etc/logrotate.d/*
ADD syslog-ng.conf /etc/syslog-ng/syslog-ng.conf
COPY crontab /etc/crontab
COPY run.sh /run.sh
COPY logrotate.conf /etc/logrotate.d/syslog-ng.conf
RUN chmod 755 /run.sh && chmod 0644 /etc/logrotate.d/syslog-ng.conf /etc/crontab

EXPOSE 514/tcp 514/udp
ENV RUN_UID 1000
ENV RUN_GID 1000
CMD exec /run.sh


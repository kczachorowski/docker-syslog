#!/bin/sh
set -e

usermod --non-unique --uid $RUN_UID loguser
groupmod --non-unique --gid $RUN_GID loguser

cron

exec /usr/sbin/syslog-ng -F -f /etc/syslog-ng/syslog-ng.conf --no-caps
